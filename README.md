# Setup instructions

This is Melos - https://melos.invertase.dev - Monorepo.

After having installed Melos following the instructions above, you can bootstrap the workspace using `melos bs`.

Any application package should be create in the `apps` folder.

Currently, 2 apps (Blue, Red) have been created and each has 3 flavors:
- development
- staging
- production

Common packages, feature packages and anything that is not the final assembly of the app should be created in `packages`.

Each package should be self contained when imported by the `app`, meaning that dependency injection and persistance should be handled internally.

Each package imported by the App should only expose configuration that should be made available to the final user of the package.

Common assets should be packaged in the individual packages, application speciifc assets should be packaged in the application package.