import 'package:flutter/material.dart';
import 'package:toggle_domain/toggle_domain.dart';
import 'package:toggle_ui/toggle_ui.dart';

class ToggleList extends StatelessWidget {
  const ToggleList(
      {Key? key,
      required this.items,
      required this.onChange,
      required this.brandColor})
      : super(key: key);
  final List<ToggleModel> items;
  final Function(ToggleModel) onChange;
  final Color brandColor;
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: items
          .map(
            (e) => Padding(
              padding: const EdgeInsets.all(8.0),
              child: Toggle(
                toggleValue: e,
                onChange: onChange,
                brandColor: brandColor,
              ),
            ),
          )
          .toList(),
    );
  }
}
