import 'package:flutter/material.dart';
import 'package:toggle_domain/models/models.dart';

class Toggle extends StatelessWidget {
  const Toggle({
    Key? key,
    required this.toggleValue,
    required this.onChange,
    required this.brandColor,
  }) : super(key: key);
  final ToggleModel toggleValue;
  final Function(ToggleModel) onChange;
  final Color brandColor;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Text(
            toggleValue.name,
          ),
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(backgroundColor: brandColor),
          onPressed: () {
            onChange(
                ToggleModel(name: toggleValue.name, value: !toggleValue.value));
          },
          child: Text("Toggle (${toggleValue.value})"),
        ),
      ],
    );
  }
}
