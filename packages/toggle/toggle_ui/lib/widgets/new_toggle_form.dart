import 'package:flutter/material.dart';
import 'package:toggle_domain/toggle_domain.dart';

class NewToggleForm extends StatefulWidget {
  const NewToggleForm({Key? key, required this.onNew, required this.brandColor})
      : super(key: key);
  final Function(ToggleModel) onNew;
  final Color brandColor;
  @override
  State<NewToggleForm> createState() => _NewToggleFormState();
}

class _NewToggleFormState extends State<NewToggleForm> {
  String? name;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: TextField(
            maxLines: 1,
            minLines: 1,
            onChanged: (val) {
              setState(() {
                name = val;
              });
            },
          ),
        ),
        ElevatedButton(
            style: ElevatedButton.styleFrom(backgroundColor: widget.brandColor),
            onPressed: name?.isNotEmpty == true
                ? () {
                    widget.onNew(ToggleModel(name: name!));
                    setState(() {
                      name = null;
                    });
                  }
                : () {},
            child: const Text("Add")),
      ],
    );
  }
}
