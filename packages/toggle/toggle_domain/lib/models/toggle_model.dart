part of 'models.dart';

class ToggleModel {
  ToggleModel({required this.name, this.value = false});
  final String name;
  final bool value;
}
