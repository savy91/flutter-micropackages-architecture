part of 'repositories.dart';

abstract class ToggleRepository {
  Future<List<ToggleModel>> getToggles();
  Future<List<ToggleModel>> addToggle(ToggleModel toggle);
  Future<List<ToggleModel>> setToggle(ToggleModel toggle);
}
