import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toggle/widgets/toggle_list/bloc/toggle_list_bloc.dart';
import 'package:toggle_ui/toggle_ui.dart' as tu;

import '../../toggle.dart';

class ToggleList extends StatelessWidget {
  const ToggleList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final brandColor = sl<ToggleFeatureConfig>().brandColor;
    return BlocProvider(
      create: (context) => sl<ToggleListBloc>()..add(LoadToggles()),
      child: BlocBuilder<ToggleListBloc, ToggleListState>(
        builder: (context, state) {
          if (state is TogglesLoaded) {
            return Column(
              children: [
                Container(
                    height: 200,
                    child: Image.asset(
                      'assets/cat.jpg',
                      fit: BoxFit.cover,
                    )),
                tu.NewToggleForm(
                    onNew: (newToggle) {
                      context
                          .read<ToggleListBloc>()
                          .add(AddToggle(toggle: newToggle));
                    },
                    brandColor: brandColor),
                Expanded(
                  child: tu.ToggleList(
                    brandColor: brandColor,
                    onChange: (newToggle) {
                      context
                          .read<ToggleListBloc>()
                          .add(ToggleToggle(toggle: newToggle));
                    },
                    items: state.toggles,
                  ),
                ),
              ],
            );
          }
          return const Text("Loading");
        },
      ),
    );
  }
}
