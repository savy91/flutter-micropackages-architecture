part of 'toggle_list_bloc.dart';

abstract class ToggleListEvent extends Equatable {
  const ToggleListEvent();
}

class LoadToggles extends ToggleListEvent {
  @override
  List<Object?> get props => [];
}

class AddToggle extends ToggleListEvent {
  const AddToggle({required this.toggle});
  final ToggleModel toggle;
  @override
  List<Object?> get props => [];
}

class ToggleToggle extends ToggleListEvent {
  const ToggleToggle({required this.toggle});
  final ToggleModel toggle;
  @override
  List<Object?> get props => [];
}
