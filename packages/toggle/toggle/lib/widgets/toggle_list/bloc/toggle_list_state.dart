part of 'toggle_list_bloc.dart';

abstract class ToggleListState extends Equatable {
  const ToggleListState();
}

class ToggleListInitial extends ToggleListState {
  @override
  List<Object> get props => [];
}

class TogglesLoaded extends ToggleListState {
  const TogglesLoaded({required this.toggles});
  final List<ToggleModel> toggles;
  @override
  List<Object> get props => [...toggles.map((e) => "${e.name}: ${e.value}")];
}
