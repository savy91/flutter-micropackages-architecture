import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toggle_domain/models/models.dart';
import 'package:toggle_domain/repositories/repositories.dart';

import '../../../toggle.dart';

part 'toggle_list_event.dart';
part 'toggle_list_state.dart';

class ToggleListBloc extends Bloc<ToggleListEvent, ToggleListState> {
  ToggleListBloc() : super(ToggleListInitial()) {
    on<LoadToggles>((event, emit) async {
      final toggles = await sl<ToggleRepository>().getToggles();
      emit(TogglesLoaded(toggles: toggles));
    });
    on<AddToggle>((event, emit) async {
      final toggles = await sl<ToggleRepository>().addToggle(event.toggle);
      emit(TogglesLoaded(toggles: toggles));
    });
    on<ToggleToggle>((event, emit) async {
      final toggles = await sl<ToggleRepository>().setToggle(event.toggle);
      emit(TogglesLoaded(toggles: toggles));
    });
  }
}
