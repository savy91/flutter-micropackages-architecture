library toggle;

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:toggle/widgets/toggle_list/bloc/toggle_list_bloc.dart';
import 'package:toggle_data/repositories/repositories.dart';
import 'package:toggle_domain/repositories/repositories.dart';

final sl = GetIt.asNewInstance();

class ToggleFeatureConfig {
  ToggleFeatureConfig({this.brandColor = Colors.blue});
  final Color brandColor;
}

class ToggleFeature {
  ToggleFeature._();
  static initialize(ToggleFeatureConfig config) {
    sl.registerSingleton<ToggleFeatureConfig>(config);
    sl.registerLazySingleton<ToggleRepository>(
        () => ToggleRepositoryInMemory());
    sl.registerFactory<ToggleListBloc>(() => ToggleListBloc());
  }
}
