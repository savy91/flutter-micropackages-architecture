import 'package:flutter/material.dart';
import 'package:red/app/app.dart';
import 'package:red/bootstrap.dart';
import 'package:toggle/toggle.dart';

void main() {
  bootstrap(() {
    ToggleFeature.initialize(ToggleFeatureConfig(brandColor: Colors.red));
    return const App();
  });
}
