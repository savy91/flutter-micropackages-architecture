import 'package:blue/app/app.dart';
import 'package:blue/bootstrap.dart';
import 'package:flutter/material.dart';
import 'package:toggle/toggle.dart';

void main() {
  bootstrap(() {
    ToggleFeature.initialize(ToggleFeatureConfig(brandColor: Colors.blue));
    return const App();
  });
}
