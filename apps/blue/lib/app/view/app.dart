import 'package:blue/l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:toggle/widgets/toggle_list/toggle_list.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        appBarTheme: const AppBarTheme(color: Colors.blue),
        colorScheme: ColorScheme.fromSwatch(
          accentColor: Colors.blue,
        ),
      ),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      home: const Scaffold(
        body: SafeArea(child: ToggleList()),
      ),
    );
  }
}
